---
title: "Monado - About Runtimes"
layout: main
---

* TOC
{:toc}

# About OpenXR runtimes

As an OpenXR runtime, Monado consists of various components necessary to implement the OpenXR API:

* XR headset and XR controller device drivers
* A compositor that knows how to show textures on XR headsets
* An implementation of the OpenXR API
* A long-running service to which OpenXR applications connect on start and disconnect on exit

# Device drivers and runtimes

OpenXR runtimes are called runtimes and not just drivers because they have a bigger scope than typical drivers. Whereas drivers are thought of to provide a rather low level abstraction to the hardware, runtimes can provide quite a bit of extended functionality. For example runtimes will often provide some means of interacting with the system while no VR application is running, like a 3D environment that allows managing and starting VR applications. Runtimes can also allow multiple VR applications to run at the same time, either switching between two applications or rendering applications composed on top of each other.

An example of a typical VR headset driver is OpenHMD. [You can read more about what OpenHMD is and what it does here](https://github.com/OpenHMD/OpenHMD/wiki/Getting-Started). The not yet released OpenXR "device plugins" will also have a similar scope of device drivers.

## What OpenXR runtimes provide

OpenXR runtimes provide standardized access to the resources of XR hardware. An incomplete list of input and output data OpenXR runtimes provide and process:

* Input
    * pose and fov of each eye (or rather an `XrView` representing a display). Projection and View matrices for rendering use can be built from this data.
    * pose of the HMD
    * `grip`, `aim` etc. poses of VR motion controllers.`XrSpace`
    * physical inputs like triggerm, touchpad, buttons, which are abstracted by an `XrAction` binding
* Output
    * a texture for each `XrView`, rendered with a standard 3D rendering workflow is presented to the HMD display
    * runtimes transparently handle direct mode. Monado supports direct mode on Intel, AMD and Nvidia GPUs.
    * runtimes transparently handle distortion correction. Monado handles OpenHMD's panotools parameters and the Vive/Index distortion parameters and turns them into a mesh.
* Since version 0.2 Monado provides a long running service that hosts the compositor and drivers
* In the future Monado will include the capability to run multiple applications at the same time and overlay them with the [XR_EXTX_overlay extension](https://www.khronos.org/registry/OpenXR/specs/1.0/html/xrspec.html#XR_EXTX_overlay)

Monado handles direct mode and extended mode similar to SteamVR. That is, if direct mode is available for a currently connected HMD, it will automatically be used. [If the graphics drivers are set up for extended mode, the Monado compositor will automatically start in extended mode](https://github.com/OpenHMD/OpenHMD/wiki/Extended-Mode).

## What Monado does not provide

* A fully end-user ready experience with all devices. See the [hardware support table](Getting-Started#monado-hardware-support) for more information.
* A drop in replacement for other VR runtimes or SDKs. To use an application with Monado, it has to be developed using the OpenXR API.
    * Examples of applications for Linux that use OpenXR: Godot games that include the [godot_openxr addon](https://github.com/GodotVR/godot_openxr), blender
    * Translation layers for other APIs to OpenXR might be implemented. For example [OpenComposite (formerly OpenOVR)](https://gitlab.com/znixian/OpenOVR) might implement an OpenXR backend, enabling the translation of OpenVR to OpenXR.
